#use wml::debian::translation-check translation="c13faedbded510feb7e5f7ea5a004d0206db58d4" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>권한 상승, 서비스 거부 또는 정보 유출을 일으킬 수 있는 여러 취약점을 리눅스 커널에서 발견했습니다.
</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8553">CVE-2015-8553</a>

    <p>Jan Beulich discovered that <a href="https://security-tracker.debian.org/tracker/CVE-2015-2150">CVE-2015-2150</a> was not completely
    addressed.  If a PCI physical function is passed through to a
    Xen guest, the guest is able to access its memory and I/O
    regions before enabling decoding of those regions.  This could
    result in a denial-of-service (unexpected NMI) on the host.</p>

    <p>The fix for this is incompatible with qemu versions before 2.5.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18509">CVE-2017-18509</a>

    <p>Denis Andzakovic reported a missing type check in the IPv4 multicast
    routing implementation. A user with the CAP_NET_ADMIN capability (in
    any user namespace) could use this for denial-of-service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5995">CVE-2018-5995</a>

    <p>ADLab of VenusTech discovered that the kernel logged the virtual
    addresses assigned to per-CPU data, which could make it easier to
    exploit other vulnerabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

    <p>chenxiang reported a race condition in libsas, the kernel
    subsystem supporting Serial Attached SCSI (SAS) devices, which
    could lead to a use-after-free.  It is not clear how this might be
    exploited.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20856">CVE-2018-20856</a>

    <p>Xiao Jin reported a potential double-free in the block subsystem,
    in case an error occurs while initialising the I/O scheduler for a
    block device.  It is not clear how this might be exploited.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

    <p>It was discovered that most x86 processors could speculatively
    skip a conditional SWAPGS instruction used when entering the
    kernel from user mode, and/or could speculatively execute it when
    it should be skipped.  This is a subtype of Spectre variant 1,
    which could allow local users to obtain sensitive information from
    the kernel or other processes.  It has been mitigated by using
    memory barriers to limit speculative execution.  Systems using an
    i386 kernel are not affected as the kernel does not use SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3882">CVE-2019-3882</a>

    <p>It was found that the vfio implementation did not limit the number
    of DMA mappings to device memory.  A local user granted ownership
    of a vfio device could use this to cause a denial of service
    (out-of-memory condition).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3900">CVE-2019-3900</a>

    <p>It was discovered that vhost drivers did not properly control the
    amount of work done to service requests from guest VMs.  A
    malicious guest could use this to cause a denial-of-service
    (unbounded CPU usage) on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

    <p>The syzkaller tool found a potential null dereference in various
    drivers for UART-attached Bluetooth adapters.  A local user with
    access to a pty device or other suitable tty device could use this
    for denial-of-service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

    <p>Amit Klein and Benny Pinkas discovered that the generation of IP
    packet IDs used a weak hash function, <q>jhash</q>.  This could enable
    tracking individual computers as they communicate with different
    remote servers and from different networks.  The <q>siphash</q>
    function is now used instead.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10639">CVE-2019-10639</a>

    <p>Amit Klein and Benny Pinkas discovered that the generation of IP
    packet IDs used a weak hash function that incorporated a kernel
    virtual address.  This hash function is no longer used for IP IDs,
    although it is still used for other purposes in the network stack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

    <p>It was discovered that the gtco driver for USB input tablets could
    overrun a stack buffer with constant data while parsing the device's
    descriptor.  A physically present user with a specially
    constructed USB device could use this to cause a denial-of-service
    (BUG/oops), or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

    <p>Praveen Pandey reported that on PowerPC (ppc64el) systems without
    Transactional Memory (TM), the kernel would still attempt to
    restore TM state passed to the sigreturn() system call.  A local
    user could use this for denial-of-service (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

    <p>The syzkaller tool found a missing bounds check in the floppy disk
    driver.  A local user with access to a floppy disk device, with a
    disk present, could use this to read kernel memory beyond the
    I/O buffer, possibly obtaining sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

    <p>The syzkaller tool found a potential division-by-zero in the
    floppy disk driver.  A local user with access to a floppy disk
    device could use this for denial-of-service (oops).</p>

<li>(CVE ID not yet assigned)

    <p>Denis Andzakovic reported a possible use-after-free in the
    TCP sockets implementation.  A local user could use this for
    denial-of-service (memory corruption or crash) or possibly
    for privilege escalation.</p></li>

<li>(CVE ID not yet assigned)

    <p>The netfilter conntrack subsystem used kernel addresses as
    user-visible IDs, which could make it easier to exploit other
    security vulnerabilities.</p></li>

<li>XSA-300

    <p>Julien Grall reported that Linux does not limit the amount of memory
    which a domain will attempt to balloon out, nor limits the amount of
    "foreign / grant map" memory which any individual guest can consume,
    leading to denial of service conditions (for host or guests).</p></li>

</ul>

<p>옛 안정 배포(stretch)에서, 이 문제를 버전 4.9.168-1+deb9u5에서 수정했습니다.</p>

<p>안정 배포(buster)에서, 이 문제를 버전 4.19.37-5+deb10u2 또는 이전에 수정했습니다.</p>

<p>linux 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>linux의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4497.data"
