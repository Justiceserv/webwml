#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités affectant le serveur d’impression cups ont été trouvées
qui peuvent conduire à l’exécution de commande IPP arbitraire et un déni de
service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18190">CVE-2017-18190</a>

<p>Une entrée de liste blanche localhost.localdomain dans valid_host() dans
scheduler/client.c dans CUPS avant 2.2.2 permet à des attaquants distants
d’exécuter des commandes arbitraires IPP en envoyant des requêtes POST au démon
de CUPS en conjonction avec un rattachement DNS. Le nom localhost.localdomain
est souvent résolu à l’aide d’un serveur DNS (ni l’OS ni le navigateur ne sont
responsables d’assurer que localhost.localdomain soit 127.0.0.1).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18248">CVE-2017-18248</a>

<p>La fonction add_job dans scheduler/ipp.c dans CUPS avant 2.2.6, lorsque la
prise en charge de D-Bus est activée, peut être plantée par des attaquants
distants en envoyant des travaux d’impression avec un nom d’utilisateur non
valable, en relation avec une notification de D-Bus.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.5-11+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets cups.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1412.data"
# $Id: $
