#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12584">CVE-2018-12584</a>
<p>Un défaut dans la fonction ConnectionBase::preparseNewBytes de
resip/stack/ConnectionBase.cxx a été détecté, qui permet à des attaquants
distants de provoquer un déni de service (dépassement de tampon) ou
éventuellement d’exécuter du code arbitraire lorsque la communication TLS est
activée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11521">CVE-2017-11521</a>

<p>Un défaut dans la fonction SdpContents::Session::Medium::parse de
resip/stack/SdpContents.cxx a été détecté, qui permet à des attaquants distants
de provoquer un déni de service (consommation de mémoire) par déclenchement de
beaucoup de connexions de médias.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:1.9.7-5+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets resiprocate.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1439.data"
# $Id: $
