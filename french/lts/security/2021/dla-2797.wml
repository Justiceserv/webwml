#use wml::debian::translation-check translation="52fa349428647aab97ee0c5616cdb3584682f52b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour comprend les modifications de tzdata 2021e. Parmi les
modifications notables :</p>

<ul>
<li> Fidji suspend l'heure d'été pour la saison 2021/2022 ;</li>
<li> la Palestine revient au 29 octobre 2021 à 01h00 pour l'heure d'hiver
(et plus le 30 octobre).</li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2021a-0+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tzdata.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tzdata, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tzdata">\
https://security-tracker.debian.org/tracker/tzdata</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :\
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2797.data"
# $Id: $
