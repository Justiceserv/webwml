#use wml::debian::translation-check translation="da004a4c84b6b888f17765a6ac013d36c580a4d4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Thunderbird,
qui pourraient aboutir à l'exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30547">CVE-2021-30547</a>

<p>Une écriture hors limites dans ANGLE dans Google Chrome avant la
version 91.0.4472.101 permet potentiellement à un attaquant distant d’accéder
hors limites à la mémoire à l’aide d’une page HTML contrefaite.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:78.12.0-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets thunderbird.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de thunderbird, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/thunderbird">\
https://security-tracker.debian.org/tracker/thunderbird</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2711.data"
# $Id: $
