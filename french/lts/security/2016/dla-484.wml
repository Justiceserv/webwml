#use wml::debian::translation-check translation="04d941142caea6346972828d64c63b95b571b8f1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans
graphicsmagick un outil pour manipuler des fichiers d'image.</p>

<p>GraphicsMagick est un fourchage d'ImageMagick et il est aussi affecté
par les vulnérabilités connues collectivement sous le nom d'ImageTragick,
qui sont la conséquence de l'absence de vérification des entrées non
fiables. Un attaquant doté du contrôle sur l'image d'entrée pourrait, avec
les droits de l'utilisateur se servant de l'application, exécuter du code
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3714">CVE-2016-3714</a>),
faire des requêtes HTTP GET ou FTP
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3718">CVE-2016-3718</a>),
ou supprimer
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3715">CVE-2016-3715</a>),
déplacer
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3716">CVE-2016-3716</a>),
ou lire
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3717">CVE-2016-3717</a>)
des fichiers locaux.</p>

<p>Pour traiter ces problèmes les modifications suivantes ont été faites :</p>

<ol>

<li>suppression de la détection ou l'exécution automatiques de MVG basées
sur l'en-tête ou l'extension du fichier ;</li>

<li>retrait de la possibilité de provoquer la suppression d'un fichier
d'entrée basée sur une spécification du nom de fichier ;</li>

<li>amélioration de la sûreté de delegates.mgk en retirant la prise en
charge de gnuplot, supprimant la prise en charge de page manuelle et en
ajoutant -dSAFER à toutes les invocations de ghostscript ;</li>

<li>vérification du nettoyage de l'argument nom de fichier premier de
l'image MVG pour s'assurer que les chaînes de préfixe « magick: » ne seront
pas interprétées. Veuillez noter que ce correctif cassera l'utilisation
intentionnelle de chaînes de préfixe magick dans MVG et ainsi certains
scripts MVG pourront échouer. Nous allons chercher une solution plus
flexible.</li>

</ol>

<p>En complément, les problèmes suivants ont été corrigés :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8808">CVE-2015-8808</a>

<p>Assurance que le décodeur GIF n'utilise pas de données non initialisées
et ne provoque pas une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2317">CVE-2016-2317</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2016-2318">CVE-2016-2318</a>

<p>Des vulnérabilités qui permettent de lire ou d'écrire en dehors des
limites de la mémoire (tas, pile) ainsi que certains déréférencements de
pointeur NULL qui provoquent un déni de service lors de l'analyse de
fichiers SVG.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.3.16-1.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-484.data"
# $Id: $
