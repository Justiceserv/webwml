#use wml::debian::translation-check translation="cce8164efea8298f3d4054656a07de3d3a87a086" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le lecteur multimédia
VLC, qui pouvaient avoir pour conséquences l'exécution de code arbitraire
ou un déni de service lors de l'ouverture d'un fichier média mal formé.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
3.0.12-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets vlc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de vlc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/vlc">\
https://security-tracker.debian.org/tracker/vlc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3050.data"
# $Id: $
