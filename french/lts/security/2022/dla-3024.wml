#use wml::debian::translation-check translation="9691b9bf2366d20ae6dfb7228498f948ee267bcb" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une potentielle vulnérabilité d'injection SQL dans le
cadriciel de développement web.</p>

<p>Des données non fiables étaient utilisées comme paramètre de tolérance
dans les fonctions et les agrégats de GIS lors de l'utilisation du moteur
de base de données Oracle. En passant un niveau de tolérance contrefait de
façon appropriée aux fonctions et agrégats de GIS sur Oracle, il était
éventuellement possible de casser l'échappement et d'injecter du code SQL
malveillant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9402">CVE-2020-9402</a>
<p>Django 1.11 avant 1.11.29, 2.2 avant 2.2.11, et 3.0 avant 3.0.4 permet
une injection SQL si des données non fiables sont utilisées comme paramètre
de tolérance dans les fonctions et les agrégats de GIS sur Oracle. En
passant un niveau de tolérance contrefait de façon appropriée aux fonctions
et agrégats de GIS sur Oracle, il était possible de casser l'échappement et
et d'injecter du code SQL malveillant.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigées dans la
version 1:1.10.7-2+deb9u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3024.data"
# $Id: $
