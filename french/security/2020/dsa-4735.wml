#use wml::debian::translation-check translation="f316e0ce25840c6590881cb5b3cec62cc137c07d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le chargeur d'amorçage
GRUB2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10713">CVE-2020-10713</a>

<p>Un défaut dans le code d'analyse de grub.cfg a été découvert qui
permettait de casser l'amorçage sécurisé (<q>Secure Boot</q>) avec
UEFI et de charger du code arbitraire. Vous trouverez plus de détails
à l'adresse <a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">\
https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14308">CVE-2020-14308</a>

<p>grub_malloc ne valide pas la taille de l'allocation permettant un
dépassement de pointeur arithmétique et par la suite un dépassement de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14309">CVE-2020-14309</a>

<p>Un dépassement d'entier dans grub_squash_read_symlink pourrait conduire
à un dépassement de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14310">CVE-2020-14310</a>

<p>Un dépassement d'entier dans read_section_from_string pourrait conduire
à un dépassement de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14311">CVE-2020-14311</a>

<p>Un dépassement d'entier dans grub_ext2_read_link pourrait conduire à un
dépassement de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15706">CVE-2020-15706</a>

<p>script : une utilisation de mémoire après libération évitée lors de la
redéfinition d'une fonction durant l'exécution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15707">CVE-2020-15707</a>

<p>Un défaut de dépassement d'entier a été découvert dans le traitement de
la taille d'initrd.</p></li>
</ul>

<p>Vous trouverez davantage d'informations détaillées à l'adresse
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">\
https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot</a>.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.02+dfsg1-20+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets grub2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de grub2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4735.data"
# $Id: $
