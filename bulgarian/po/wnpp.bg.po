#
# Damyan Ivanov <dmn@debian.org>, 2011, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.53\n"
"PO-Revision-Date: 2012-05-20 20:25+0300\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Български <dict@fsa-bg.org>\n"
"Language: Bulgarian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Няма заявки за осиновяване"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Няма изоставени пакети"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Няма пакети, чакащи за осиновяване"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Няма пакети, чакащи да бъдат пакетирани"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Няма заявени пакети"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Няма молби за помощ"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "в процес на осиновяване от днес."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "в процес на осиновяване от вчера."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "в процес на осиновяване от %s дни."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "в процес на осиновяване от %s дни, последна промяна днес."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "в процес на осиновяване от %s дни, последна промяна вчера."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr "в процес на осиновяване от %s дни, последна промяна преди %s дни."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "в процес на подготовка от днес."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "в процес на подготовка от вчера."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "в процес на подготовка от %s дни."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "в процес на подготовка от %s дни, последна промяна днес."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "в процес на подготовка от %s дни, последна промяна вчера."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "в процес на подготовка от %s дни, последна промяна преди %s дни."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "поискано осиновяване днес."

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "поискано осиновяване вчера."

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "поискано осиновяване преди %s дни."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "изоставен днес."

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "изоставен вчера."

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "изоставен преди %s дни."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "заявен днес."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "заявен вчера."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "заявен преди %s дни."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "информация за пакета"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "популярност:"
