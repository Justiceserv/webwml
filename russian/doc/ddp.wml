#use wml::debian::ddp title="Проект Документации Debian"
#use wml::debian::translation-check translation="e8de3f9543c6c16d99765f9d2b60925bd6495dd0" maintainer="Lev Lamberov"

<p>Проект Документирования Debian был создан для координирования и объединения
всех усилий по написанию документации лучшего качества и большого количества
по системе Debian.</p>

  <h2>Результаты работы проекта</h2>
<div class="line">
  <div class="item col50">

    <h3>Руководства</h3>
    <ul>
      <li><strong><a href="user-manuals">Руководства для пользователей</a></strong></li>
      <li><strong><a href="devel-manuals">Руководства для разработчиков</a></strong></li>
      <li><strong><a href="misc-manuals">Различные руководства</a></strong></li>
      <li><strong><a href="#other">Менее полезные руководства</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Политика документирования</h3>
    <ul>
      <li>Лицензии документов совместимы с DFSG.</li>
      <li>Для наших документов используется Docbook XML.</li>
      <li>Исходный код должен быть размещён по адресу <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a></li>
      <li><tt>www.debian.org/doc/&lt;имя-руководства&gt;</tt> является официальным URL документации</li>
      <li>Каждый документ должен активно сопровождаться.</li>
      <li>Если вы хотите написать новую документацию, спросите об этом в списке рассылки <a href="https://lists.debian.org/debian-doc/">debian-doc</a></li>
    </ul>

    <h3>Доступ к Git</h3>
    <ul>
      <li><a href="vcs">Как получить доступ</a> к git-репозиторию DDP</li>
    </ul>

  </div>


</div>

<hr />

<h2><a name="other">Менее полезные руководства</a></h2>

<p>Помимо указанных руководств, мы сопровождаем нижеследующие руководства,
с которыми связаны те или иные проблемы. Из-за этих проблем мы не рекомендуем
пользователям работать с ними. Мы вас предостерегли.</p>

<ul>
  <li><a href="obsolete#tutorial">Учебник по Debian</a>, устарел</li>
  <li><a href="obsolete#guide">Руководство по Debian</a>, устарел</li>
  <li><a href="obsolete#userref">Справочное руководство пользователя
      Debian</a>, разработка остановлена, достаточно неполон</li>
  <li><a href="obsolete#system">Руководство системного администратора
      Debian</a>, разработка остановлена, почти пусто</li>
  <li><a href="obsolete#network">Руководство администратора сети
      Debian</a>, разработка остановлена, неполно</li>
  <li><a href="obsolete#swprod">Как разработчики ПО могут распространять
      свою продукцию непосредственно в формате .deb</a>, разработка остановлена, устарел</li>
  <li><a href="obsolete#packman">Руководство по созданию пакетов Debian</a>,
      частично включено в <a href="devel-manuals#policy">Руководство по
      политике Debian</a>, остальная часть будет включена в справочное
      руководство по dpkg, находящееся в стадии написания</li>
  <li><a href="obsolete#makeadeb">Введение: создание пакета Debian</a>,
      устарело после появления
      <a href="devel-manuals#maint-guide">Руководства нового сопровождающего Debian</a></li>
  <li><a href="obsolete#programmers">Руководство программиста Debian</a>,
      устарело после появления
      <a href="devel-manuals#maint-guide">Руководства нового сопровождающего Debian</a>
      и
      <a href="devel-manuals#debmake-doc">Руководства для сопровождающих Debian</a></li>
  <li><a href="obsolete#repo">Руководство по репозиториям Debian</a>, устарело после
      введения безопасного APT</li>
  <li><a href="obsolete#i18n">Введение в интернационализацию (i18n)</a>, разработка остановлена</li>
  <li><a href="obsolete#sgml-howto">Руководство по Debian SGML/XML</a>, разработка остановлена, устарел</li>
  <li><a href="obsolete#markup">Руководство по разметке Debiandoc-SGML</a>, разработка остановлена; DebianDoc будет удалён</li>
</ul>
