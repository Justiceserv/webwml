<define-tag pagetitle>Debian 11 aktualisiert: 11.4 veröffentlicht</define-tag>
<define-tag release_date>2022-07-09</define-tag>
#use wml::debian::news
# $Id:
#use wml::debian::translation-check translation="ab5fb9a77e8b85994504923f86d139f3a7bb9f00" maintainer="Erik Pfannenstein"

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die vierte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung bringt hauptsächlich Korrekturen für
Sicherheitsprobleme und Anpassungen für einige ernste Probleme. Für sie sind
bereits separate Sicherheitsankündigungen veröffentlicht worden; auf diese
wird, wo möglich, verwiesen.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version
von Debian <release> darstellt, sondern nur einige der enthaltenen Pakete
auffrischt. Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da
deren Pakete nach der Installation mit Hilfe eines aktuellen
Debian-Spiegelservers auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht
viele Pakete auf den neuesten Stand bringen müssen. Die meisten
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>


<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apache2 "Neue stabile Veröffentlichung der Originalautoren; Problem mit HTTP-Anfrageschmuggel behoben [CVE-2022-26377], Probleme mit Lesezugriff außerhalb der Grenzen [CVE-2022-28330 CVE-2022-28614 CVE-2022-28615], Dienstblockaden [CVE-2022-29404 CVE-2022-30522], möglicher Lesezugriff außerhalb der Grenzen [CVE-2022-30556], mögliche IP-basierte Authentifizierungs-Umgehung [CVE-2022-31813]">
<correction base-files "/etc/debian_version auf die Zwischenveröffentlichung 11.4 aktualisiert">
<correction bash "Einlesen des ersten Bytes eines überlaufenden Puffers behoben, das zu fehlerhaften Multibyte-Zeichen in den Befehlssubstitutionen geführt hat">
<correction clamav "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction clementine "Fehlende Abhängigkeit von libqt5sql5-sqlite nachgetragen">
<correction composer "Anfälligkeit für Code-Injektion behoben [CVE-2022-24828]; GitHub-Token-Muster aktualisiert">
<correction cyrus-imapd "Sicherstellen, dass alle Mailboxen ein <q>uniqueid</q>-Feld haben, um Probleme beim Upgrade auf 3.6 zu lösen">
<correction dbus-broker "Pufferüberlauf behoben [CVE-2022-31212]">
<correction debian-edu-config "Mail aus dem lokalen Netzwerk an root@&lt;meine-netzwerk-namen&gt; annehmen; Kerberos-Host und -Dienstprinzipiale nur erzeugen, wenn sie nicht schon existieren; sicherstellen, dass libsss-sudo auf Roaming Workstations installiert ist; Benennung und Sichtbarkeit von Druckerwarteschlangen überarbeitet; krb5i auf Diskless Workstations unterstützen; squid: DNSv4-Lookups DNSv6-Lookups vorziehen">
<correction debian-installer "Neukompilierung gegen proposed-updates; Linux-Kernel-ABI auf 16 angehoben; einige armel-Netboot-Targets (openrd) wiederhergestellt">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates; Linux-Kernel-ABI auf 16 angehoben; einige armel-Netboot-Targets (openrd) wiederhergestellt">
<correction distro-info-data "Ubuntu 22.10, Kinetic Kudu, hinzugefügt">
<correction docker.io "docker.service hinter containerd.service einordnen, um den Herunterfahrprozess der Container zu verbessern; den containerd-Socket-Pfad explizit an dockerd übergeben, um sicherzustellen, dass es selbst keinen containerd startet">
<correction dpkg "dpkg-deb: Bedingungen für unerwartete Dateienden in .deb-Entpackung überarbeitet; libdpkg: Virtuelle source:*-Felder nicht auf installierte Pakete beschränken; Dpkg::Source::Package::V2: Immer die Berechtigungen der Tarballs der Originalautoren korrigieren (R DSA-5147-1]">
<correction freetype "Pufferüberlauf behoben [CVE-2022-27404]; Abstürze beseitigt [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Pufferüberläufe behobens [CVE-2022-25308 CVE-2022-25309]; Absturz beseitigt [CVE-2022-25310]">
<correction ganeti "Neue Veröffentlichung der Originalautoren; verschiedene Upgrade-Probleme behoben; Live-Migration mit QEMU 4 und <q>user</q> oder <q>pool</q> als <q>security_model</q> verbessert">
<correction geeqie "Strg-Klick innerhalb einer Blockauswahl überarbeitet">
<correction gnutls28 "SSSE3-SHA384-Fehlberechnung korrigiert; Nullzeiger-Dereferenzierung behoben [CVE-2021-4209]">
<correction golang-github-russellhaering-goxmldsig "Nullzeiger-Dereferenzierung behoben, die durch manipulierte XML-Signaturen verursacht wird [CVE-2020-7711]">
<correction grunt "Verzeichnisüberschreitung behoben [CVE-2022-0436]">
<correction hdmi2usb-mode-switch "udev: Suffix an Geräteknoten in /dev/video anhängen, um sie besser unterscheidbar zu machen; Priorität der udev-Regeln auf 70 geändert, damit 60-persistent-v4l.rules vor ihnen an der Reihe ist">
<correction hexchat "Fehlende Abhängigkeit von python3-cffi-backend nachgetragen">
<correction htmldoc "Endlosschleife [CVE-2022-24191], Ganzzahlüberläufe [CVE-2022-27114] und Heap-Puffer-Überläufe behoben [CVE-2022-28085]">
<correction knot-resolver "Möglichen Assertionsfehlschlag in NSEC3-Grenzfall behoben [CVE-2021-40083]">
<correction libapache2-mod-auth-openidc "Neue stabile Veröffentlichung der Originalautoren; offene Weiterleitung behoben [CVE-2021-39191]; Absturz beim Neuladen/Neustarten behoben">
<correction libintl-perl "gettext_xs.pm tatsächlich installieren">
<correction libsdl2 "Lesezugriff außerhalb der Grenzen vermeiden, wenn eine beschädigte BMP-Datei geladen wird [CVE-2021-33657] sowie während der YUV-auf-RGB-Umwandlung">
<correction libtgowt "Neue stabile Veröffentlichung der Originalautoren, um neueres telegram-desktop zu unterstützen">
<correction linux "Neue stabile Veröffentlichung der Originalautoren; ABI auf 16 angehoben">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren; ABI auf 16 angehoben">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren; ABI auf 16 angehoben">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren; ABI auf 16 angehoben">
<correction logrotate "Sperrung überspringen, wenn das State-File für alle lesbar ist [CVE-2022-1348]; bei der Konfigurations-Auswertung strenger vorgehen, um zu vermeiden, dass fremde Dateien wie Speicherauszüge eingelesen werden">
<correction lxc "Standard-GPG-Key-Server aktualisiert, um die Erstellung von Containern mit der <q>download</q>-Vorlage zu verbessern">
<correction minidlna "HTTP-Anfragen validieren, um vor DNS-Rebinding-Angriffen sicher zu sein [CVE-2022-26505]">
<correction mutt "Pufferüberlauf in uudecode behoben [CVE-2022-1328]">
<correction nano "Mehrere Fehlerkorrekturen, darunter auch welche gegen Abstürze">
<correction needrestart "cgroup-Erkennung von Diensten und Benutzersitzungen überarbeitet, damit sie auf cgroup v2 anspricht">
<correction network-manager "Neue stabile Veröffentlichung der Originalautoren">
<correction nginx "Absturz, wenn libnginx-mod-http-lua geladen ist und init_worker_by_lua* verwendet wird, behoben; Angriff durch Vertauschung von Anwendungsschicht-Protokoll-Inhalten im Mail-Modul [CVE-2021-3618]">
<correction node-ejs "Serverseitige Vorlagen-Injektion behoben [CVE-2022-29078]">
<correction node-eventsource "Heikle Kopfzeilen vor dem Weiterleiten woandershin entfernen [CVE-2022-1650]">
<correction node-got "Keine Weiterleitung an Unix-Socket erlauben [CVE-2022-33987]">
<correction node-mermaid "Seitenübergreifendes Skripting behoben [CVE-2021-23648 CVE-2021-43861]">
<correction node-minimist "Prototype Pollution behoben [CVE-2021-44906]">
<correction node-moment "Verzeichnisüberschreitung behoben [CVE-2022-24785]">
<correction node-node-forge "Signaturverifizierungsprobleme behoben [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-raw-body "Potenzielle Dienstblockade in node-express durch Verwendung von node-iconv-lite statt node-iconv behoben">
<correction node-sqlite3 "Dienstblockade behoben [CVE-2022-21227]">
<correction node-url-parse "Authentifizierungs-Umgehung behoben [CVE-2022-0686 CVE-2022-0691]">
<correction nvidia-cuda-toolkit "OpenJDK8-Schnappschüsse für amd64 und ppc64el verwenden; Verwendbarkeit der Java-Binärdatei kontrollieren; nsight-compute: <q>sections</q>-Verzeichnis an einen Multiarach-Ort verschieben; nvidia-openjdk-8-jre-Versionsreihenfolge korrigiert">
<correction nvidia-graphics-drivers "Neue Veröffentlichung der Originalautoren; Umstellung auf 470-Baum der Originalautoren; Dienstblockade behoben [CVE-2022-21813 CVE-2022-21814]; Schreibzugriff außerhalb der Grenzen [CVE-2022-28181], Lesezugriff außerhalb der Grenzen [CVE-2022-28183], Dienstblockade [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192] behoben">
<correction nvidia-graphics-drivers-legacy-390xx "Neue Veröffentlichung der Originalautoren; Schreibzugriff außerhalb der Grenzen abgestellt [CVE-2022-28181 CVE-2022-28185]">
<correction nvidia-graphics-drivers-tesla-418 "Neue stabile Veröffentlichung der Originalautoren">
<correction nvidia-graphics-drivers-tesla-450 "Neue stabile Veröffentlichung der Originalautoren; Schreibzugriff außerhalb der Grenzen abgestellt [CVE-2022-28181 CVE-2022-28185], denial of service issue [CVE-2022-28192]">
<correction nvidia-graphics-drivers-tesla-460 "Neue stabile Veröffentlichung der Originalautoren">
<correction nvidia-graphics-drivers-tesla-470 "Neues Paket, das die Tesla-Unterstützung auf den 470-Baum umstellt; Schreibzugriff außerhalb der Grenzen [CVE-2022-28181], Lesezugriff außerhalb der Grenzen [CVE-2022-28183], Dienstblockade [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192] behoben">
<correction nvidia-persistenced "Neue Veröffentlichung der Originalautoren; Umstellung auf 470-Baum der Originalautoren">
<correction nvidia-settings "Neue Veröffentlichung der Originalautoren; Umstellung auf 470-Baum der Originalautoren">
<correction nvidia-settings-tesla-470 "Neues Paket, das die Tesla-Unterstützung auf den 470-Baum umstellt">
<correction nvidia-xconfig "Neue Veröffentlichung der Originalautoren">
<correction openssh "seccomp: pselect6_time64-Systemaufruf auf 32-Bit-Architekturen einführen">
<correction orca "Verwendung mit webkitgtk 2.36 korrigiert">
<correction php-guzzlehttp-psr7 "Ungeeignete Kopfzeilenauswertung korrigiert [CVE-2022-24775]">
<correction phpmyadmin "Einige SQL-Abfragen überarbeitet, die zu einem Serverfehler geführt haben">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren; nicht den vom Benutzer gesetzten default_transport in postinst überschreiben; if-up.d: nicht mit Fehler aussteigen, wenn postfix noch keine Mail verschicken kann">
<correction procmail "Nullzeigerdereferenzierung behoben">
<correction python-scrapy "Nicht mit allen Anfragen Authentifizierungsdaten mitschicken [CVE-2021-41125]; bei Weiterleitung keine Cookies domainübergreifend vorzeigen [CVE-2022-0577]">
<correction ruby-net-ssh "Authentifizierung gegenüber Sytemen mit OpenSSH 8.8 überarbeitet">
<correction runc "seccomp defaultErrnoRet berücksichtigen; keine vererbbaren Fähigkeiten setzen [CVE-2022-29162]">
<correction samba "Startfehlschlag von winbind, wenn <q>allow trusted domains = no</q> verwendet wird, behoben; MIT-Kerberos-Authentifizierung überarbeitet; Anfälligkeit für Ausbruch aus dem Freigabeverzeichnis via Race-Condition beim Anlegen eines Verzeichnisses behoben [CVE-2021-43566]; mögliche ernste Datenkorrumpierung wegen Cache-Vergiftung bei Windows-Client behoben; Installation auf Systemen ohne systemd überarbeitet">
<correction tcpdump "AppArmor-Profil aktualisiert, um den Zugriff auf *.cap-Dateien und den Umgang mit Zahlen-Suffixen in Dateinamen, die durch -W hinzugefügt wurden, zu ermöglichen">
<correction telegram-desktop "Neue stabile Veröffentlichung der Originalautoren, um die Funktionalität wiederherzustellen">
<correction tigervnc "Start des GNOME-Desktops, wenn tigervncserver@.service verwendet wird, überarbeitet; Farbdarstellung korrigiert, wenn vncviewer und X11-Server verschiedene Endians benutzen">
<correction twisted "Informationsoffenlegung bei domainübergreifenden Weiterleitungen [CVE-2022-21712], Dienstblockade während SSH-Handshakes [CVE-2022-21716], HTTP-Anfrageschmuggel [CVE-2022-24801] beseitigt">
<correction tzdata "Zeitzonendaten für Palästina aktualisiert; Schaltsekundenliste aktualisiert">
<correction ublock-origin "Neue stabile Veröffentlichung der Originalautoren">
<correction unrar-nonfree "Verzeichnisüberschreitung abgestellt [CVE-2022-30333]">
<correction usb.ids "Neue Veröffentlichung der Originalautoren; mitgelieferte Daten aktualisiert">
<correction wireless-regdb "Neue Veröffentlichung der Originalautoren; Umleitung entfernt, die vom Installer angelegt wurde, um sicherzustellen, dass die Dateien aus dem Paket verwendet werden">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 4999 asterisk>
<dsa 2021 5026 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5107 php-twig>
<dsa 2022 5108 tiff>
<dsa 2022 5110 chromium>
<dsa 2022 5111 zlib>
<dsa 2022 5112 chromium>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5114 chromium>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5116 wpewebkit>
<dsa 2022 5117 xen>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5120 chromium>
<dsa 2022 5121 chromium>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5124 ffmpeg>
<dsa 2022 5125 chromium>
<dsa 2022 5127 linux-signed-amd64>
<dsa 2022 5127 linux-signed-arm64>
<dsa 2022 5127 linux-signed-i386>
<dsa 2022 5127 linux>
<dsa 2022 5128 openjdk-17>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5130 dpdk>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5133 qemu>
<dsa 2022 5134 chromium>
<dsa 2022 5136 postgresql-13>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5148 chromium>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5155 wpewebkit>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5161 linux-signed-amd64>
<dsa 2022 5161 linux-signed-arm64>
<dsa 2022 5161 linux-signed-i386>
<dsa 2022 5161 linux>
<dsa 2022 5162 containerd>
<dsa 2022 5163 chromium>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5166 slurm-wlm>
<dsa 2022 5167 firejail>
<dsa 2022 5168 chromium>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5174 gnupg2>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>


<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction elog "Unbetreut; Sicherheitsprobleme">
<correction python-hbmqtt "Unbetreut und defekt">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt; oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>

