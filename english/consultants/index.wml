#use wml::debian::template title="Debian Consultants" MAINPAGE="true" NOCOMMENTS="yes" GENTIME="yes"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian lists consultants as a courtesy to its users and does not endorse any of the people or companies listed. Entries are the sole responsibility of the corresponding consultant. Please see the <a href="info">consultant information page</a> for our policy of adding or updating a listing.</p>
</aside>

<p>
Debian is free software and offers free help through various <a href="../MailingLists/">mailing lists</a>. Some people either don't have the time or have specialized needs and are willing to hire someone to either maintain or add additional functionality to their Debian system. We therefore offer a list of people, organization, and companies making at least part of their income from providing <strong>paid</strong> Debian support.
</p>

<p>
The list is sorted by country, names within each country are simply listed in the order received. If your country is missing, please check any neighboring countries, as some of the consultants will work internationally or remotely.
</p> 

<p>There are additional lists of consultants for specific uses of Debian:</p>

<ul>
<li><a href="https://wiki.debian.org/DebianEdu/Help/ProfessionalHelp">DebianEdu</a>:
for the use of Debian in schools, universities and other educational settings
<li><a href="https://wiki.debian.org/LTS/Funding">Debian LTS</a>:
for long term security support of Debian
</ul>

<hrline>

<h2>Countries with Debian consultants:</h2>

#include "../../english/consultants/consultant.data"

<hrline>

# left for backwards compatibility - in case someone still links to #policy
# which is fairly unlikely, but hey
<h2><a name="policy"></a>Policy for Debian's consultants page</h2>
<p>Please see the <a href="info">page with information for consultants</a>
for our policy of adding or updating a listing.
</p>
