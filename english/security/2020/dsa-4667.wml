<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

    <p>Paulo Bonzini discovered that the KVM implementation for Intel
    processors did not properly handle instruction emulation for L2
    guests when nested virtualization is enabled.  This could allow
    an L2 guest to cause privilege escalation, denial of service,
    or information leaks in the L1 guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8428">CVE-2020-8428</a>

    <p>Al Viro discovered a use-after-free vulnerability in the VFS
    layer.  This allowed local users to cause a denial-of-service
    (crash) or obtain sensitive information from kernel memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

    <p>It was discovered that the vhost_net driver did not properly
    validate the type of sockets set as back-ends.  A local user
    permitted to access /dev/vhost-net could use this to cause a stack
    corruption via crafted system calls, resulting in denial of
    service (crash) or possibly privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

    <p>Entropy Moe reported that the shared memory filesystem (tmpfs) did
    not correctly handle an <q>mpol</q> mount option specifying an empty
    node list, leading to a stack-based out-of-bounds write.  If user
    namespaces are enabled, a local user could use this to cause a
    denial of service (crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11884">CVE-2020-11884</a>

    <p>Al Viro reported a race condition in memory management code for
    IBM Z (s390x architecture), that can result in the kernel
    executing code from the user address space.  A local user could
    use this for privilege escalation.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.19.98-1+deb10u1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4667.data"
# $Id: $
