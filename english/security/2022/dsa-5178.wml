<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for some types of Intel CPUs and
provides mitigations for security vulnerabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21127">CVE-2022-21127</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a>

    <p>Various researchers discovered flaws in Intel processors,
    collectively referred to as MMIO Stale Data vulnerabilities, which
    may result in information leak to local users.</p>

    <p>For details please refer to
    <a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/processor-mmio-stale-data-vulnerabilities.html">\
    https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/processor-mmio-stale-data-vulnerabilities.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21151">CVE-2022-21151</a>

    <p>Alysa Milburn, Jason Brandt, Avishai Redelman and Nir Lavi
    discovered that for some Intel processors optimization removal or
    modification of security-critical code may result in information
    disclosure to local users.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 3.20220510.1~deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 3.20220510.1~deb11u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5178.data"
# $Id: $
