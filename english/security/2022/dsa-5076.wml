<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Security researchers of JFrog Security and Ismail Aydemir discovered two remote
code execution vulnerabilities in the H2 Java SQL database engine which can be
exploited through various attack vectors, most notably through the H2 Console
and by loading custom classes from remote servers through JNDI. The H2 console
is a developer tool and not required by any reverse-dependency in Debian. It
has been disabled in (old)stable releases. Database developers are advised to
use at least version 2.1.210-1, currently available in Debian unstable.</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 1.4.197-4+deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.4.197-4+deb11u1.</p>

<p>We recommend that you upgrade your h2database packages.</p>

<p>For the detailed security status of h2database please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/h2database">\
https://security-tracker.debian.org/tracker/h2database</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5076.data"
# $Id: $
