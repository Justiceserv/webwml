<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues were discovered in <tt>openexr</tt>, a set of tools to manipulate
OpenEXR image files, often in the computer-graphics industry for visual effects
and animation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16588">CVE-2020-16588</a>

    <p>A Null Pointer Deference issue exists in Academy Software Foundation
    OpenEXR 2.3.0 in generatePreview in makePreview.cpp that can cause a denial
    of service via a crafted EXR file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16589">CVE-2020-16589</a>

    <p>A head-based buffer overflow exists in Academy Software Foundation
    OpenEXR 2.3.0 in writeTileData in ImfTiledOutputFile.cpp that can cause a
    denial of service via a crafted EXR file.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.2.0-11+deb9u2.</p>

<p>We recommend that you upgrade your openexr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2491.data"
# $Id: $
