<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in Graphicsmagick, a
collection of image processing tools. Heap-based buffer over-reads and
a memory leak may lead to a denial-of-service or information disclosure.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.20-3+deb8u6.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1755.data"
# $Id: $
