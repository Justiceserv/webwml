<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues in elfutils, a collection of utilities to handle ELF
objects, have been found either by fuzzing or by using an
AddressSanitizer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7665">CVE-2019-7665</a>

     <p>Due to a heap-buffer-overflow problem in function elf32_xlatetom()
     a crafted ELF input can cause segmentation faults.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7150">CVE-2019-7150</a>

     <p>Add sanity check for partial core file dynamic data read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7149">CVE-2019-7149</a>

     <p>Due to a heap-buffer-overflow problem in function read_srclines()
     a crafted ELF input can cause segmentation faults.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18521">CVE-2018-18521</a>

     <p>By using a crafted ELF file, containing a zero sh_entsize, a
     divide-by-zero vulnerability could allow remote attackers to
     cause a denial of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18520">CVE-2018-18520</a>

     <p>By fuzzing an Invalid Address Deference problem in function elf_end
     has been found.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18310">CVE-2018-18310</a>

     <p>By fuzzing an Invalid Address Read problem in eu-stack has been
     found.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16062">CVE-2018-16062</a>

     <p>By using an AddressSanitizer a heap-buffer-overflow has been found.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7613">CVE-2017-7613</a>

     <p>By using fuzzing it was found that an allocation failure was not
     handled properly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7612">CVE-2017-7612</a>

     <p>By using a crafted ELF file, containing an invalid sh_entsize, a
     remote attackers could cause a denial of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7611">CVE-2017-7611</a>

     <p>By using a crafted ELF file a remote attackers could cause a denial
     of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7610">CVE-2017-7610</a>

     <p>By using a crafted ELF file a remote attackers could cause a denial
     of service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7608">CVE-2017-7608</a>

     <p>By fuzzing a heap based buffer overflow has been detected.</p></li>

</ul>

<p>For Debian 8 &quot;Jessie&quot;, these problems have been fixed in version
0.159-4.2+deb8u1.</p>

<p>We recommend that you upgrade your elfutils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1689.data"
# $Id: $
