<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25219">CVE-2021-25219</a>

    <p>Kishore Kumar Kothapalli discovered that the lame server cache in BIND,
    a DNS server implementation, can be abused by an attacker to
    significantly degrade resolver performance, resulting in denial of
    service (large delays for responses for client queries and DNS timeouts
    on client hosts).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5740">CVE-2018-5740</a>

    <p>"deny-answer-aliases" is a little-used feature intended to help recursive
     server operators protect end users against DNS rebinding attacks, a
     potential method of circumventing the security model used by client
     browsers. However, a defect in this feature makes it easy, when the
     feature is in use, to experience an assertion failure in name.c.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:9.10.3.dfsg.P4-12.3+deb9u10.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2807.data"
# $Id: $
